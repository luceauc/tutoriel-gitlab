Ajout de la clé publique à GitLab
================

Il est supposé que git.exe a bien été installer sur votre ordinateur et qu'une clef RSA a été générée en 
suivant ce [tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md)

Note importante
------------

Les ports SSH semblent bloqués au sein de la DREES. Un simple passage par https au lieu de SSH permet de 
s'affranchir de cette 
limite lors d'un ```git clone``` par exemple. L'ajout de la clef RSA est donc probablement inutile.

Ajout de la clé
--------------

S'identifier sur GitLab.com et aller dans "Settings"

![1_settings](https://gitlab.com/DREES/tutoriels/raw/master/img/gitlab_key/1_settings.PNG)

Cliquer sur "Settings" après avoir cliquer sur la boîte Profil en haut à droite de l'écran.

![2_rsa](https://gitlab.com/DREES/tutoriels/raw/master/img/gitlab_key/2_rsa.PNG)

Cliquer sur l'onglet SSH Keys.

![3_zone](https://gitlab.com/DREES/tutoriels/raw/master/img/gitlab_key/3_zone.PNG)

Il faut copier votre publique dans la zone "Key". Pour se faire il faut lancer "Git Bash"

![4_git_bash](https://gitlab.com/DREES/tutoriels/raw/master/img/gitlab_key/4_git_bash.PNG)

Lancer la commande suivante depuis "Git Bash": ```cat ~/.ssh/id_rsa.pub | clip``` 

Vous pouvez directement la copier et faire un ```Inser``` dans votre "Git Bash"

Désormais votre clé est dans votre presse papier, un ```Ctrl+V``` la copiera.

![5_gut](https://gitlab.com/DREES/tutoriels/raw/master/img/gitlab_key/5_gut.PNG)

Cliquer dans la zone et faire ```Ctrl+V```. La clé publique devrait apparaître sous un format équivalent. 
Donner un titre pour la retrouver. Ajouter la clé publique en cliquant sur "Add Keys".

La clé RSA publique est désormais ajoutée à GitLab.com et devrait permettre d'utiliser git via SSH si les 
ports ne sont pas bloqués par le parefeu.
